<?php

class Local_Learn_Block_Welcome extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('learn/index.phtml');
    }
}