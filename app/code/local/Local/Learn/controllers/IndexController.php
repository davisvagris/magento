<?php

class Local_Learn_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        return $this->renderLayout();
    }
}