<?php
$this->startSetup();
$this->run("
    -- DROP TABLE IF EXISTS {$this->getTable('Magebit_ProductComments')};
    CREATE TABLE `{$this->getTable('Magebit_ProductComments')}` (
      `comment_id` INTEGER NOT NULL AUTO_INCREMENT DEFAULT NULL,
      `product_id` INTEGER NOT NULL,
      `customer_id` INTEGER NOT NULL,
      `message` MEDIUMTEXT NOT NULL,
      `created_at` DATETIME NOT NULL,
      PRIMARY KEY (`comment_id`)
    );
");
$this->endSetup();