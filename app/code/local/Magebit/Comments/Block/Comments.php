<?php

class Magebit_Comments_Block_Comments extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('magebit/comments/comment.phtml');
    }

    /**
     * Get customer
     *
     * @return array
     */
    public function getCustomer()
    {
        return (bool) Mage::helper('magebit_comments/comment')->getCustomer();
    }

    /**
     * Get current product
     *
     * @return mixed
     */
    public function getProduct()
    {
        $product = Mage::registry('current_product');
        return (object) array(
            'id' => $product->getId(),
            'allowedComment' => (bool) $product->getData('isAllowedComment')
        );
    }

    /**
     * @return Mage_Core_Model_Abstract
     */
    public function getComments()
    {
        $commentCollection = Mage::getModel('comments/comment')->getCollection();
        $commentCollection->getSelect()
                        ->joinLeft('customer_entity', 'customer_id = entity_id', array('email'))
                        ->where('product_id = ?', $this->getProduct()->id);

        return $commentCollection;
    }

    /**
     * Get validation error
     *
     * @return mixed
     */
    public function errors()
    {
        $session = Mage::getSingleton('core/session');
        $hasError = $session->hasErrorMessage();

        if (!empty($hasError)) {
            $message = $session->getErrorMessage();
            $session->unsErrorMessage();
            return $message;
        }

        return false;
    }
}