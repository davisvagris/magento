<?php

class Magebit_Comments_Helper_Comment extends Mage_Core_Helper_Abstract {

    /**
     * Get customer
     *
     * @return mixed|null
     */
    public function getCustomer()
    {
        $customer = null;
        $customerSession = Mage::getSingleton('customer/session');
        if ($customerSession->isLoggedIn()) {
            $customer = $customerSession->getCustomer()->getId();
        }

        return $customer;
    }
}