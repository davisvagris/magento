<?php

class Magebit_Comments_Model_Comment extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('comments/comment');
    }
}