<?php

class Magebit_Comments_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * @return bool
     * @throws Exception
     */
    public function indexAction()
    {
        $post = $this->getRequest()->getPost();
        if (empty($post)) {
            return $this->_redirectReferer();
        }

        if ($this->validate($post)) {
            return $this->_redirectReferer();
        }

        $data = array(
            'message' => $post['message'],
            'product_id' => $post['product'],
            'customer_id' => Mage::helper('magebit_comments/comment')->getCustomer(),
            'created_at' => now()
        );

        Mage::getModel('comments/comment')->setData($data)->save();
        Mage::getSingleton('core/session')->addSuccess('Comment successful added');
        return $this->_redirectReferer();
    }

    /**
     * Validate all rules
     *
     * @param $data
     * @return bool
     *
     * @throws Zend_Validate_Exception
     */
    protected function validate($data)
    {
        $errors = array();
        if (!Zend_Validate::is($data['message'], 'NotEmpty')) {
            $errors['message'] = 'Please field message';
        }

        $products = Mage::getModel('catalog/product')->load($data['product']);
        if (!Zend_Validate::is(!empty($products->getId()), 'NotEmpty')) {
            $errors['product'] = "Can't find product";
        }

        if (count($errors) > 0) {
            Mage::getSingleton('core/session')->setErrorMessage($errors);
            return true;
        }

        return false;
    }
}