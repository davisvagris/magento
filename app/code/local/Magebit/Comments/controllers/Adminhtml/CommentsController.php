<?php

class Magebit_Comments_Adminhtml_CommentsController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()
             ->_setActiveMenu('magebit_comments/comments')
             ->_title('Magebit comments');

        $block = $this->getLayout()->createBlock('magebit_comments/adminhtml_comment');
        var_dump($block);

        return $this->renderLayout();
    }
}