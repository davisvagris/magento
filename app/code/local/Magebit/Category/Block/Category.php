<?php

class Magebit_Category_Block_Category extends Mage_Core_Block_Template
{
    /**
     * Magebit_Category_Block_Category constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('magebit/category/list.phtml');
    }

    /**
     * Get category
     *
     * @return mixed
     */
    public function getCategory()
    {
        return Mage::getModel('catalog/category')->getCategories($this->getData('category'));
    }

    /**
     * @TODO set on category from xml
     */
    public function setCategory()
    {
    }
}